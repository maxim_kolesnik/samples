#include <string.h>           //memset, memcmp
#include "ObjectAllocator.h"  //OAException, ObjectAllocator


/******************************************************************************/
/*!

  \brief Creates an instance of object manager

  \param ObjectSize
    Size of an object to allocate
  \param config
    Configuration settings

*/
/******************************************************************************/
ObjectAllocator::ObjectAllocator(size_t ObjectSize, const OAConfig &config)
  : m_headPageList(NULL), m_headFreeList(NULL), m_config(config), 
  m_objectSize(ObjectSize)
{
  unsigned pSize = static_cast<unsigned>(sizeof(GenericObject *));
  unsigned hSize = static_cast<unsigned>(m_config.HBlockInfo_.size_);

  m_stats.ObjectSize_ = ObjectSize;

  //Calculate page size
  m_stats.PageSize_
    = pSize
    +
    ObjectSize * config.ObjectsPerPage_                 //Objects
    +
    2 * config.PadBytes_ * config.ObjectsPerPage_       //Padding
    +
    hSize * config.ObjectsPerPage_;                     //Headers

  //Calculate allignment
  if (m_config.Alignment_ > 1)
  {
    unsigned startOfPage = pSize + m_config.PadBytes_ + hSize;
    unsigned allign = m_config.Alignment_;

    m_config.LeftAlignSize_ = allign - (startOfPage % allign);

    unsigned objBlock = static_cast<unsigned>(m_stats.ObjectSize_) 
      + 2 * m_config.PadBytes_ + hSize;

    m_config.InterAlignSize_ = allign - (objBlock % allign);

    m_stats.PageSize_ += m_config.LeftAlignSize_
      + m_config.InterAlignSize_ * (m_config.ObjectsPerPage_ - 1);
  }

  AllocateNewPage();
}

/******************************************************************************/
/*!

  \brief Destroys object allocator


*/
/******************************************************************************/
ObjectAllocator::~ObjectAllocator()
{
  GenericObject *tempPage = m_headPageList;

  while (tempPage)
  {
    //If object has external header deallocate its memory
    if (m_config.HBlockInfo_.type_ == OAConfig::hbExternal)
    {
      char *object = reinterpret_cast<char*>(tempPage)+sizeof(GenericObject*);
      for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
      {
        MemBlockInfo **header = reinterpret_cast<MemBlockInfo**>(object);
        if (*header != NULL)
        {
          if ((*header)->label != NULL)
            delete[](*header)->label;
          delete *header;
        }
        object += 2 * m_config.PadBytes_ + m_config.HBlockInfo_.size_ + m_stats.ObjectSize_;
      }
    }
    //Delete page
    GenericObject *free = tempPage;
    tempPage = tempPage->Next;
    delete[] reinterpret_cast<char*>(free);
  }
}

/******************************************************************************/
/*!

  \brief Allocate memory

  \param label
    Label for memory block

  \return pointer to allocated memory

*/
/******************************************************************************/
void *ObjectAllocator::Allocate(const char *label)
{
  if (m_config.UseCPPMemManager_ == true)
  {
    UpdateAllocStats();
    return new char[m_stats.ObjectSize_];
  }

  if (m_stats.FreeObjects_ == 0)
    AllocateNewPage();
  
  //Take a memory block from the beginnig of free list
  GenericObject *alloc = m_headFreeList;
  m_headFreeList = m_headFreeList->Next;

  memset(reinterpret_cast<void*>(alloc), ALLOCATED_PATTERN, m_stats.ObjectSize_);

  //Update stats and header
  UpdateAllocStats();
  UpdateHeader(reinterpret_cast<char*>(alloc), m_config.HBlockInfo_.type_, label);

  return alloc;
}

/******************************************************************************/
/*!

  \brief Deallocates memory

  \param Object
    pointer to memory block to free

  \exception OAException
    object on a bad boundary
  \exception OAException
    multiple frees
*/
/******************************************************************************/
void ObjectAllocator::Free(void *Object)
{
  if (m_config.UseCPPMemManager_ == true)
  {
    ++m_stats.Deallocations_;
    delete[] reinterpret_cast<char*>(Object);
    return;
  }

  if (m_config.DebugOn_)
  {
    if (!IsValidPointer(Object))
      throw OAException(OAException::E_BAD_BOUNDARY, "Free: object wasn't found\n");
    if (DoubleFreeCheck(Object))
      throw OAException(OAException::E_MULTIPLE_FREE, "Free: multiple frees\n");
    CheckPadBytes(reinterpret_cast<char*>(Object));
  }

  PutOnFreeList(reinterpret_cast<char*>(Object));
  UpdateDeallocStas();
  ClearHeader(reinterpret_cast<char*>(Object));
}

/******************************************************************************/
/*!

  \brief Dumps memory for every block of memory in use

  \param fn
    pointer to a function to print memory

  \return
    number of objects in use

*/
/******************************************************************************/
unsigned ObjectAllocator::DumpMemoryInUse(DUMPCALLBACK fn) const
{
  GenericObject *pageList = m_headPageList;
  //Offsets
  size_t offsetToObject = sizeof(GenericObject*) + m_config.PadBytes_
    + m_config.HBlockInfo_.size_;
  size_t nextObjOffset = m_stats.ObjectSize_ + 2 * m_config.PadBytes_
    + m_config.HBlockInfo_.size_;

  while (pageList)
  {
    char *objectList = reinterpret_cast<char*>(pageList) + offsetToObject;

    for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
    {
      if (IsObjectInUse(objectList))
        fn(objectList, m_stats.ObjectSize_);
      objectList += nextObjOffset;
    }
    pageList = pageList->Next;
  }

  return m_stats.ObjectsInUse_;
}

/******************************************************************************/
/*!

  \brief Check pages for corruption

  \param fn
    Pointer to function to print memory if it was corrupted

  \return
    Number of corruptions

*/
/******************************************************************************/
unsigned ObjectAllocator::ValidatePages(VALIDATECALLBACK fn) const
{
  if (m_config.DebugOn_ == false || m_config.PadBytes_ == 0)
    return 0;

  unsigned numCorruptions = 0;

  GenericObject *pageList = m_headPageList;
  //Offsets
  size_t offsetToObject   = sizeof(GenericObject*) + m_config.HBlockInfo_.size_ 
                                                   + m_config.PadBytes_;
  size_t nextObjOffset    = 2 * m_config.PadBytes_ + m_config.HBlockInfo_.size_
                                                   + m_stats.ObjectSize_;
  size_t backPadOff       = m_stats.ObjectSize_;
  size_t frontPadOff      = m_config.PadBytes_;

  while (pageList)
  {
    char *objectList = reinterpret_cast<char*>(pageList) + offsetToObject;

    for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
    {
      //Offsets for pad bytes
      char *frontPad = objectList - frontPadOff;
      char *backPad  = objectList + backPadOff;

      if (memcmp(frontPad, backPad, m_config.PadBytes_) != 0)
      {
        fn(objectList, m_stats.ObjectSize_);
        ++numCorruptions;
      }
      objectList += nextObjOffset;
    }
    pageList = pageList->Next;
  }

  return numCorruptions;
}

/******************************************************************************/
/*!

  \brief Frees empty pages

  \return
    Number of pages freed

*/
/******************************************************************************/
unsigned ObjectAllocator::FreeEmptyPages(void)
{
  GenericObject *page = m_headPageList;
  GenericObject *prev = NULL;
  unsigned pagesFreed = 0;

  while (page)
  {
    if (IsPageEmpty(page))
    {
      //In case we are removing head
      if (prev == NULL)
      {
        prev = m_headPageList;
        page = m_headPageList = m_headPageList->Next;
        RemovePageFromFreeList(prev);
        delete[] prev;
        prev = NULL;
      }
      else
      {
        prev->Next = page->Next;
        RemovePageFromFreeList(page);
        delete[] page;
        page = prev->Next;
      }

      ++pagesFreed;
      --m_stats.PagesInUse_;
      continue;
    }

    prev = page;
    page = page->Next;
  }
  return pagesFreed;
}

/******************************************************************************/
/*!

  \return
    If extra credit was implemented

*/
/******************************************************************************/
bool ObjectAllocator::ImplementedExtraCredit(void)
{
  return true;
}

/******************************************************************************/
/*!

  \brief Sets debug state

  \param State
    State of debug to state

*/
/******************************************************************************/
void ObjectAllocator::SetDebugState(bool State)
{
  m_config.DebugOn_ = State;
}

/******************************************************************************/
/*!

  \brief Returns head to the free list

  \return
    Head to the free list

*/
/******************************************************************************/
const void *ObjectAllocator::GetFreeList(void) const
{
  return m_headFreeList;
}

/******************************************************************************/
/*!

  \brief Returns head to the page list

  \return
    Head to the page list

*/
/******************************************************************************/
const void *ObjectAllocator::GetPageList(void) const
{
  return m_headPageList;
}

/******************************************************************************/
/*!

  \brief Returns config

  \return
    Config of the allocator

*/
/******************************************************************************/
OAConfig ObjectAllocator::GetConfig(void) const
{
  return m_config;
}

/******************************************************************************/
/*!

  \brief Returns statistics

  \return
    Statistics of allocator

*/
/******************************************************************************/
OAStats ObjectAllocator::GetStats(void) const
{
  return m_stats;
}

/////////////////////////////PRIVATE METHODS////////////////////////////////////

/******************************************************************************/
/*!

  \brief Allocates new page

  \exception OAException
    if out of logical memory
  \exception OAException
    if system cannot allocate memory

*/
/******************************************************************************/
void ObjectAllocator::AllocateNewPage(void)
{
  if (m_stats.PagesInUse_ == m_config.MaxPages_ && m_config.MaxPages_ != 0)
    throw OAException(OAException::E_NO_PAGES, "AllocateNewPage: out of logical"
    "memory\n");

  char *newPage;

  //Allocate page
  try
  {
    newPage = new char[m_stats.PageSize_];
  }
  catch (std::bad_alloc &)
  {
    throw OAException(OAException::E_NO_MEMORY, "AllocateNewPage: system can"
      "not allocate memory\n");
  }

  memset(newPage, UNALLOCATED_PATTERN, m_stats.PageSize_);

  //Put in a PageList
  reinterpret_cast<GenericObject *>(newPage)->Next = m_headPageList;
  m_headPageList = reinterpret_cast<GenericObject*>(newPage);

  ++m_stats.PagesInUse_;

  //Put obects on a FreeList
  newPage += sizeof(GenericObject*);
  //Left Allignment
  memset(newPage, ALIGN_PATTERN, m_config.LeftAlignSize_);
  newPage += m_config.LeftAlignSize_;

  for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
  {
    if (i != 0)
    {
      memset(newPage, ALIGN_PATTERN, m_config.InterAlignSize_);
      newPage += m_config.InterAlignSize_;
    }
    //Header
    memset(newPage, 0, m_config.HBlockInfo_.size_);
    newPage += m_config.HBlockInfo_.size_;
    //Front Padding
    memset(newPage, PAD_PATTERN, m_config.PadBytes_);
    newPage += m_config.PadBytes_;
    //Free list
    reinterpret_cast<GenericObject *>(newPage)->Next = m_headFreeList;
    m_headFreeList = reinterpret_cast<GenericObject *>(newPage);
    newPage += m_stats.ObjectSize_;
    //Back padding
    memset(newPage, PAD_PATTERN, m_config.PadBytes_);
    newPage += m_config.PadBytes_;
  }

  m_stats.FreeObjects_ += m_config.ObjectsPerPage_;
}

/******************************************************************************/
/*!

  \brief Check if provided pointer is valid

  \param Object
    Pointer to memory to free

  \return
    true if pointer is valid

*/
/******************************************************************************/
bool ObjectAllocator::IsValidPointer(void *Object) const
{
  GenericObject *pageList = m_headPageList;
  //Offsets
  size_t offsetToObject = sizeof(GenericObject*) + m_config.HBlockInfo_.size_
    + m_config.PadBytes_ + m_config.LeftAlignSize_;
  size_t nextObjOffset = 2 * m_config.PadBytes_ + m_config.HBlockInfo_.size_
    + m_stats.ObjectSize_ + m_config.InterAlignSize_;

  while (pageList)
  {
    char *objectList = reinterpret_cast<char*>(pageList)+offsetToObject;

    for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
    {
      if (objectList == Object)
        return true;
      objectList += nextObjOffset;
    }
    pageList = pageList->Next;
  }

  return false;
}

/******************************************************************************/
/*!

  \brief Puts deallocated memory ob the free list

  \param Object
    Pointer to memory

*/
/******************************************************************************/
void ObjectAllocator::PutOnFreeList(char *Object)
{
  memset(Object, FREED_PATTERN, m_stats.ObjectSize_);

  GenericObject *obj = reinterpret_cast<GenericObject*>(Object);
  obj->Next = m_headFreeList;
  m_headFreeList = obj;
}

/******************************************************************************/
/*!

  \brief Checks if memory is being freed twice

  \param Object
    Pointer to memory to check

  \return
    true if memory is being freed twice

*/
/******************************************************************************/
bool ObjectAllocator::DoubleFreeCheck(void *Object) const
{
  GenericObject *list = m_headFreeList;

  while (list)
  {
    if (list == Object)
      return true;

    list = list->Next;
  }

  return false;
}

/******************************************************************************/
/*!

  \brief Updates header of allocated object

  \param Object
    Pointer to object
  \param type
    Type of a header to update
  \param label
    Label to write into header

*/
/******************************************************************************/
void ObjectAllocator::UpdateHeader(char *Object, OAConfig::HBLOCK_TYPE type, 
  const char *label)
{
  if (type == OAConfig::hbBasic)
  {
    Object -= m_config.HBlockInfo_.size_ + m_config.PadBytes_;
    unsigned int *allocs = reinterpret_cast<unsigned int*>(Object);
    *allocs = m_stats.Allocations_;

    Object += sizeof(unsigned int);
    bool *inUse = reinterpret_cast<bool*>(Object);
    *inUse = true;
  }
  else if (type == OAConfig::hbExtended)
  {
    Object -= m_config.HBlockInfo_.size_ + m_config.PadBytes_;

    Object += m_config.HBlockInfo_.additional_;
    unsigned short *useCounter = reinterpret_cast<unsigned short*>(Object);
    (*useCounter)++;

    Object += sizeof(unsigned short);
    unsigned int *allocs = reinterpret_cast<unsigned int*>(Object);
    *allocs = m_stats.Allocations_;

    Object += sizeof(unsigned int);
    bool *inUse = reinterpret_cast<bool*>(Object);
    *inUse = true;
  }
  else if (m_config.HBlockInfo_.type_ == OAConfig::hbExternal)
  {
    Object -= m_config.HBlockInfo_.size_ + m_config.PadBytes_;

    MemBlockInfo **header = reinterpret_cast<MemBlockInfo**>(Object);
    try
    {
      *header = new MemBlockInfo;
      if (label != NULL)
        (*header)->label = new char[strlen(label) + 1];
      else
        (*header)->label = NULL;
    }
    catch (std::bad_alloc &)
    {
      throw OAException(OAException::E_NO_MEMORY, "AllocateNewPage: system can"
        "not allocate memory\n");
    }

    (*header)->alloc_num = m_stats.Allocations_;
    if (label != NULL)
      strncpy((*header)->label, label, strlen(label) + 1);
    (*header)->in_use = true;
  }
}

/******************************************************************************/
/*!

  \brief Clears a header of freed object

  \param object
    Pointer to object

*/
/******************************************************************************/
void ObjectAllocator::ClearHeader(char *object)
{

  if (m_config.HBlockInfo_.type_ == OAConfig::hbBasic)
  {
    char *header = object - (m_config.HBlockInfo_.size_ + m_config.PadBytes_);

    memset(header, 0, m_config.HBlockInfo_.size_);
  }
  else if (m_config.HBlockInfo_.type_ == OAConfig::hbExtended)
  {
    char *header = object - (m_config.HBlockInfo_.size_ + m_config.PadBytes_);

    size_t sizeToUseC = m_config.HBlockInfo_.additional_ + sizeof(unsigned short);
    header += sizeToUseC;

    memset(header, 0, m_config.HBlockInfo_.size_ - sizeToUseC);
  }
  else if (m_config.HBlockInfo_.type_ == OAConfig::hbExternal)
  {
    object -= (m_config.HBlockInfo_.size_ + m_config.PadBytes_);
    MemBlockInfo **header = reinterpret_cast<MemBlockInfo**>(object);

    if ((*header)->label != NULL)
      delete[](*header)->label;
    delete *header;

    memset(object, 0, sizeof(MemBlockInfo*));
  }
}

/******************************************************************************/
/*!

  \brief Checks if padding bytes are the same

  \param Object
    Pointer to memory

  \exception
    if memory was corrupten

*/
/******************************************************************************/
void ObjectAllocator::CheckPadBytes(char *Object) const
{
  if (m_config.DebugOn_ == false)
    return;

  char *frontPad = Object - m_config.PadBytes_;
  char *backPad  = Object + m_stats.ObjectSize_;

  if (memcmp(frontPad, backPad, m_config.PadBytes_) != 0)
    throw OAException(OAException::E_CORRUPTED_BLOCK, "AllocateNewPage: memory"
      "was corrupted\n");
}

/******************************************************************************/
/*!

  \brief Updates allocation stats

*/
/******************************************************************************/
void ObjectAllocator::UpdateAllocStats(void)
{
  ++m_stats.Allocations_;
  --m_stats.FreeObjects_;
  ++m_stats.ObjectsInUse_;

  if (m_stats.ObjectsInUse_ > m_stats.MostObjects_)
    ++m_stats.MostObjects_;
}

/******************************************************************************/
/*!

  \brief Updates deallocation stats

*/
/******************************************************************************/
void ObjectAllocator::UpdateDeallocStas(void)
{
  ++m_stats.FreeObjects_;
  ++m_stats.Deallocations_;
  --m_stats.ObjectsInUse_;
}

/******************************************************************************/
/*!

  \brief Checks if memory block is in use

  \param Object
    Pointer to memory to free

  \return
    true if block is being used

*/
/******************************************************************************/
bool ObjectAllocator::IsObjectInUse(char *Object) const
{
  Object -= m_config.PadBytes_;
  
  if (m_config.HBlockInfo_.type_ == OAConfig::hbExternal)
  {
    Object -= m_config.HBlockInfo_.size_;
    MemBlockInfo **info = reinterpret_cast<MemBlockInfo**>(Object);
    return (*info) ? (*info)->in_use : false;
  }
  else
  {
    Object -= sizeof(bool);
    bool inUse =  *reinterpret_cast<bool*>(Object);

    return inUse;
  }
}

/******************************************************************************/
/*!

  \brief Checks if page is empty

  \param page
    Pointer to a page

  \return
    true if page is empty

*/
/******************************************************************************/
bool ObjectAllocator::IsPageEmpty(GenericObject *page) const
{
  //Offsets
  size_t offsetFromStart = sizeof(GenericObject*) + m_config.LeftAlignSize_
    + m_config.HBlockInfo_.size_ + m_config.PadBytes_;
  size_t objOffset = m_stats.ObjectSize_ + 2 * m_config.PadBytes_
    + m_config.HBlockInfo_.size_ + m_config.InterAlignSize_;

  char *obj = reinterpret_cast<char*>(page)+offsetFromStart;
  for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
  {
    unsigned char *data = reinterpret_cast<unsigned char*>(obj + sizeof(GenericObject*));
    if (*data != FREED_PATTERN && *data != UNALLOCATED_PATTERN)
      return false;

    obj += objOffset;
  }

  return true;
}

/******************************************************************************/
/*!

  \brief Removes page from object manager

  \param page
    Page to free

*/
/******************************************************************************/
void ObjectAllocator::RemovePageFromFreeList(GenericObject *page)
{
  size_t offsetFromStart = sizeof(GenericObject*) + m_config.LeftAlignSize_
    + m_config.HBlockInfo_.size_ + m_config.PadBytes_;
  size_t objOffset = m_stats.ObjectSize_ + 2 * m_config.PadBytes_
    + m_config.HBlockInfo_.size_ + m_config.InterAlignSize_;

  char *obj = reinterpret_cast<char*>(page)+offsetFromStart;
  for (unsigned i = 0; i < m_config.ObjectsPerPage_; ++i)
  {
    RemoveObjFromFreeList(obj);

    obj += objOffset;
  }
}

/******************************************************************************/
/*!

  \brief Removes object from free list

  \param Object
    Pointer to an object to remove

*/
/******************************************************************************/
void ObjectAllocator::RemoveObjFromFreeList(char *Object)
{
  GenericObject *list = m_headFreeList;
  GenericObject *prev = NULL;

  while (list)
  {
    if (reinterpret_cast<char*>(list) == Object)
    {
      if (prev == NULL)
      {
        prev = m_headFreeList;
        list = m_headFreeList = m_headFreeList->Next;
        prev = NULL;
      }
      else
      {
        prev->Next = list->Next;
        list = prev->Next;
      }
      --m_stats.FreeObjects_;
      continue;
    }
    prev = list;
    list = list->Next;
  }
}
