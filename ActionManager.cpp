/*!****************************************************************************
\file    CodeSmaples
\author  Maxim Kolesnik (maxim.kolesnik)
******************************************************************************/

////////////////////////////Undo/Redo Action manager//////////////////////////

ActionList *ActionManager::s_actionList = nullptr;
ActionManager *ActionManager::s_instance = nullptr;
const std::string ActionManager::s_tempFilesFolder = "TempEditorFiles";
const std::string ActionManager::s_tempActions = "Actions";

ActionManager::ActionManager(void) : m_undoButton(nullptr), m_redoButton(nullptr)
{
  s_actionList = new ActionList(c_numOfActions);

  //Connect events
  GET_GIZMO_MANAGER->e_beginManipulation += e_newManip;
  GET_GIZMO_MANAGER->e_manipulating += e_manipulating;
  
  CreateDirectories();
}

ActionManager::~ActionManager(void)
{
  char buffer[255];
  GetCurrentDirectory(255, buffer);
  DeleteDirectory(std::string(buffer) + "\\" + s_tempFilesFolder, true);

  delete s_actionList;
  delete m_undoButton;
  delete m_redoButton;
}

ActionManager* ActionManager::GetInstance(void)
{
  if (!s_instance)
    s_instance = new ActionManager();

  return s_instance;
}

//Selection action
void ActionManager::SelectObjects(dataStructures::Array<hndl> &trans)
{
  s_actionList->PushBack(new SelectionChanged(trans));
  UpdateActions();

#ifdef _DEBUG
  editor::DisplayWarning("Selection Action");
#endif
}

//Create object action
void ActionManager::CreateObject(const QString &archName)
{
  hndl obj = editor::g_dockManager->GetArchetypeBrowser()->GetArchetypeList()->CreateObjectFromArchetype(archName);
  editor::g_gameWindow->ForceGameUpdate();

  if (obj != INVALID_HANDLE)
  {
    dataStructures::Array<hndl> trans;
    trans.PushBack(GET_TRANSFORM_DATA->GetComponent(obj));

    //Focus and consume gizmo manager
    GET_GIZMO_MANAGER->FocusObjects(trans);
    GET_GIZMO_MANAGER->ConsumeUpdate();

    editor::g_dockManager->GetObjectBrowser()->GetObjectList()->SetSelectedObjects(QVector<hndl>(obj), false);

    //Record
    s_actionList->PushBack(new CreateObjectAction(obj, archName));
    UpdateActions();

    editor::g_dockManager->GetObjectBrowser()->GetObjectList()->RefreshList();

    editor::DisplayWarning("CreateObject\n");
  }
}

//Removes objects and saves action
void ActionManager::RemoveObjects(dataStructures::Array<hndl> &objHndls)
{
  if (objHndls.GetSize())
  {
    std::list<std::pair<hndl, Json::Value*> > actionInfo;

    //remove all children first
    std::list<hndl> objToRemove;
    RemoveChildren(objHndls, objToRemove);

    //Serialize and destroy
    for (auto &it : objToRemove)
    {
      Json::Value *ser = new Json::Value;
      auto &obj = GET_OBJECT_MANAGER->GetObject(it);

      coreSpace::SerializeHierarchy(it, *ser);
      actionInfo.push_back(std::make_pair<hndl, Json::Value*&>(obj.GetParent(), ser));
      GET_OBJECT_MANAGER->DestroyObject(it, 0);
    }

    //Get directory and set flag
    std::string dir = GetAvailableSaveDirectory();
    m_actionDirPool[dir] = false;

    //Record
    s_actionList->PushBack(new RemoveObjectAction(actionInfo, dir, objToRemove));
    UpdateActions();

    //Deallocate Values
    for (auto &it : actionInfo)
      delete it.second;
  }
}

//Saves object suplication
void ActionManager::DuplicateObjects(dataStructures::Array<hndl> &objToCopy,
  dataStructures::Array<hndl> newObjs)
{
  s_actionList->PushBack(new DuplicateObjectsAction(objToCopy, newObjs));
  UpdateActions();

#ifdef _DEBUG
  editor::DisplayWarning("Duplicate Action");
#endif
}

//Parents objects and saves action
void ActionManager::ParentObjects(dataStructures::Array<hndl> &objects, hndl newParent)
{
  dataStructures::Array<ParentObjectAction::ParentActionInfo> info;

  //Collect action information
  for (auto &obj : objects)
  {
    hndl oldParent = GET_OBJECT_MANAGER->GetObject(obj).GetParent();
    GET_OBJECT_MANAGER->GetObject(obj).SetParent(newParent);
    info.PushBack(ParentObjectAction::ParentActionInfo(obj, oldParent));
  }

  //Record
  s_actionList->PushBack(new ParentObjectAction(info, newParent));
  UpdateActions();

  //Refresh widgets
  editor::g_dockManager->GetObjectBrowser()->GetObjectList()->RefreshList();
  editor::g_dockManager->GetPropertyEditor()->RefreshPropertyTree();
}

//Parents object and saves action
void ActionManager::ParentObject(hndl object, hndl newParent)
{
  dataStructures::Array<ParentObjectAction::ParentActionInfo> info;

  //Save information
  hndl oldParent = GET_OBJECT_MANAGER->GetObject(object).GetParent();
  GET_OBJECT_MANAGER->GetObject(object).SetParent(newParent);
  info.PushBack(ParentObjectAction::ParentActionInfo(object, oldParent));

  //Record
  s_actionList->PushBack(new ParentObjectAction(info, newParent));
  UpdateActions();

  //Refresh widgets
  editor::g_dockManager->GetObjectBrowser()->GetObjectList()->RefreshList();
  editor::g_dockManager->GetPropertyEditor()->RefreshPropertyTree();
}

//Records component modification
void ActionManager::RecordCompModification(const dataStructures::Array<hndl> &comps,
  const dataStructures::Array<coreSpace::DynamicEntity> &initialVals,
  const dataStructures::Array<coreSpace::DynamicEntity> &newVals, 
  scriptSpace::SetterFunc setter)
{
  const QVector<hndl> &selection = 
    editor::g_dockManager->GetObjectBrowser()->GetObjectList()->GetSelectedObjects();

  dataStructures::Array<hndl> selectedObjs;
  for (const auto &it : selection)
    selectedObjs.PushBack(it);

  //Record
  s_actionList->PushBack(new ModifyComponentAction(comps, initialVals, newVals,
    selectedObjs, setter));
  UpdateActions();

  GET_GIZMO_MANAGER->UpdateFocus();

#ifdef _DEBUG
  editor::DisplayWarning("Component Modified Action");
#endif
}

//Records component modification
void ActionManager::RecordCompModification(const dataStructures::Array<hndl> &comps,
  const dataStructures::Array<coreSpace::DynamicEntity> &initialVals,
  const dataStructures::Array<coreSpace::DynamicEntity> &newVals,
  uint32 dataName)
{
  const QVector<hndl> &selection =
    editor::g_dockManager->GetObjectBrowser()->GetObjectList()->GetSelectedObjects();

  dataStructures::Array<hndl> selectedObjs;
  for (const auto &it : selection)
    selectedObjs.PushBack(it);

  //Record
  s_actionList->PushBack(new ModifyComponentAction(comps, initialVals, newVals,
    selectedObjs, dataName));
  UpdateActions();

  GET_GIZMO_MANAGER->UpdateFocus();

#ifdef _DEBUG
  editor::DisplayWarning("Data Container Modified Action");
#endif
}

//Adds component and records
void ActionManager::AddComponent(const std::string &compName, 
  const dataStructures::Array<hndl> &objects)
{
  // Extra step if trying to add a collider
  if(compName.find("Collider") != std::string::npos)
  {
    AddComponent("Rigidbody", objects);
  }

  //Add component to selected objects
  int size = objects.GetSize();
  for (int i = 0; i < size; ++i)
  {
    luabridge::LuaRef *args[8] = { nullptr };
    scriptSpace::LuaDataManager::CallAttachFunc(compName.c_str(), objects[i], args);
  }

  //Record
  s_actionList->PushBack(new AddComponentAction(compName, objects));
  UpdateActions();
}

//Removes component and records
void ActionManager::RemoveComponent(const QVector<hndl> &objects, 
  const std::string &compName)
{
  // Removes all colliders when a rigidbody is removed
  if(compName.find("Rigidbody") != std::string::npos)
  {
    RemoveComponent(objects, "BoxCollider");
    RemoveComponent(objects, "SphereCollider");
    RemoveComponent(objects, "ConvexHullCollider");
  }

  std::unordered_map<hndl, Json::Value> actionInfo;

  //Serialize and remove
  int size = objects.size();
  for (int i = 0; i < size; ++i)
  {
    Json::Value ser;
    hndl handle = objects[i];

    coreSpace::SerializeHierarchy(handle, ser);
    actionInfo[handle] = ser;

    scriptSpace::LuaDataManager::CallDetachFunc(compName, handle);
  }

  //Record
  const std::string &saveDir = GetAvailableSaveDirectory();
  s_actionList->PushBack(new RemoveComponentAction(actionInfo, compName, saveDir));
  UpdateActions();

  editor::g_dockManager->GetPropertyEditor()->RefreshPropertyTree();
}

//Removes ComponentData type
void ActionManager::RemoveComponentData(const QVector<hndl> &objects, 
  const QVector<hndl> &comps, uint32 dataName)
{
  QVector<Json::Value> jsonVals;
  QVector<hndl> objsFromComps;

  //Collect information
  for (auto &it : comps)
    objsFromComps.push_back(GET_COMPONENT_DATA(coreSpace::DataContainerComponentData)->GetObject(it));

  //Serialize all and remove
  int size = objsFromComps.size();
  for (int i = 0; i < size; ++i)
  {
    //Serialize
    Json::Value ser;
    hndl handle = objsFromComps[i];
    coreSpace::SerializeHierarchy(handle, ser);
    jsonVals.push_back(ser);

    //Remove
    auto& currContainer = GET_COMPONENT_DATA(coreSpace::DataContainerComponentData)->m_dataEntityContainer[comps[i]];
    uint32 size = currContainer.GetCapacity();
    for (uint32 i = 0; i < size; ++i)
    {
      if (currContainer.IsAlive(i) && currContainer[i].m_name == dataName)
      {
        currContainer.Remove(i);
      }
    }
  }

  //Record and update toolbar
  const std::string &saveDir = GetAvailableSaveDirectory();
  s_actionList->PushBack(new RemoveCompDataAction(comps, jsonVals, objsFromComps, dataName, saveDir));
  UpdateActions();

  editor::g_dockManager->GetPropertyEditor()->RefreshPropertyTree();
}

//Undo toolvar click
void ActionManager::Undo(void)
{
  const IUndoRedoAction* action = s_actionList->PrevAction();
  if (action)
  {
    action->Undo();
    SetFocus();
    UpdateActions();
#ifdef _DEBUG
    editor::DisplayWarning("Undo " + action->GetLabel());
#endif
  }
}

//Redo toolbar click
void ActionManager::Redo(void)
{
  const IUndoRedoAction* action = s_actionList->NextAction();
  if (action)
  {
    action->Redo();
    SetFocus();
    UpdateActions();
#ifdef _DEBUG
    editor::DisplayWarning("Redo " + action->GetLabel());
#endif
  }
}

//Resets action manager removing saved data
//For example, it will be called on loading new level
void ActionManager::Reset(void)
{
  delete s_actionList;
  s_actionList = new ActionList(c_numOfActions);
  for (auto &it : m_actionDirPool)
    it.second = true;

  m_undoButton->GetAction()->setIcon(QIcon(":images/undo_icon_faded.png"));
  m_undoButton->GetAction()->setEnabled(false);
  m_redoButton->GetAction()->setIcon(QIcon(":images/redo_icon_faded.png"));
  m_redoButton->GetAction()->setEnabled(false);
}

//Return a list of actions it is possible to undo
QList<ActionListWidgetItem*> ActionManager::GetUndoList(void)
{
  QList<ActionListWidgetItem*> list;
  ActionList::ActionNode *temp = s_actionList->m_current;

  //Return if there are no actions recorded
  //Dummy action node does not count as an action
  if (s_actionList->m_current == nullptr
      ||
      s_actionList->m_current == s_actionList->m_lastDeleted)
    return list;

  while (temp != s_actionList->m_lastDeleted)
  {
    list.push_back(new ActionListWidgetItem(QString(temp->m_action->GetLabel().c_str()),
      temp->m_action));

    temp = temp->m_prev;
  }

  return list;
}

//Return a list of actions it is possible to Redo
QList<ActionListWidgetItem*> ActionManager::GetRedoList(void)
{
  QList<ActionListWidgetItem*> list;
  ActionList::ActionNode *temp = s_actionList->m_current;

  //Return if there are no actions recorded
  //or all actions were redone
  if (s_actionList->m_current == nullptr || temp == s_actionList->m_tail)
    return list;

  temp = temp->m_next;
  while (temp != nullptr)
  {
    list.push_back(new ActionListWidgetItem(QString(temp->m_action->GetLabel().c_str()),
      temp->m_action));
    

    temp = temp->m_next;
  }

  return list;
}

//Undo items selected from drop down list
void ActionManager::UndoItem(ActionListWidgetItem *item)
{
  const IUndoRedoAction* action = s_actionList->PrevAction();
  while (action != item->m_action)
  {
    if (action)
      action->Undo();
    action = s_actionList->PrevAction();
  }

  //Focus on the action which is current
  if (action)
  {
    action->Undo();
    action->Focus();
  }

  UpdateActions();
}

//Redo items selected from drop down list
void ActionManager::RedoItem(ActionListWidgetItem *item)
{
  const IUndoRedoAction* action = s_actionList->NextAction();
  while (action != item->m_action)
  { 
    if (action)
      action->Redo();

    action = s_actionList->NextAction();
  }

  //Focus on the action which is current
  if (action)
  {
    action->Redo();
    action->Focus();
  }

  UpdateActions();
}

//Create an action, displayed on the toolbar
UndoRedoButton* ActionManager::CreateUndoAction(const QString &actionName, QWidget *parent)
{
  if (m_undoButton != nullptr)
  {
    editor::DisplayError("Action Manager has undo button action allocated");
    return nullptr;
  }

  m_undoButton = new UndoRedoButton(actionName, parent, UndoRedoButton::ActionType::Undo);
  m_undoButton->GetAction()->setIcon(QIcon(":images/undo_icon_faded.png"));
  m_undoButton->GetAction()->setEnabled(false);

  return m_undoButton;
}

//Create an action, displayed on the toolbar
UndoRedoButton* ActionManager::CreateRedoAction(const QString &actionName, QWidget *parent)
{
  if (m_redoButton != nullptr)
  {
    editor::DisplayError("Action Manager has undo button action allocated");
    return nullptr;
  }

  m_redoButton = new UndoRedoButton(actionName, parent, UndoRedoButton::ActionType::Redo);
  m_redoButton->GetAction()->setIcon(QIcon(":images/redo_icon_faded.png"));
  m_redoButton->GetAction()->setEnabled(false);

  return m_redoButton;
}

//Return undo action, displayed on the toolbar
QAction* ActionManager::GetUndoButtonAction(void)
{ 
  if (m_undoButton == nullptr)
  {
    editor::DisplayError("Action manager button is not initialized");
    return nullptr;
  }

  return m_undoButton->GetAction(); 
}

//Return redo action, displayed on the toolbar
QAction* ActionManager::GetRedoButtonAction(void)
{ 
  if (m_redoButton == nullptr)
  {
    editor::DisplayError("Action manager button is not initialized");
    return nullptr;
  }

  return m_redoButton->GetAction(); 
}

//Returns widget, hosting undo action
QWidget* ActionManager::GetUndoButtonWidget(void)
{
  if (m_undoButton == nullptr)
  {
    editor::DisplayError("Action manager button is not initialized");
    return nullptr;
  }

  return m_undoButton;
}

//Returns widget, hosting redo action
QWidget* ActionManager::GetRedoButtonWidget(void)
{
  if (m_redoButton == nullptr)
  {
    editor::DisplayError("Action manager button is not initialized");
    return nullptr;
  }

  return m_redoButton;
}

//Records new manipulation
void ActionManager::e_newManip(dataStructures::Array<hndl> &transforms,
  gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE type)
{
  switch (type)
  {
  //Translation
  case gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE::EVT_TRANS:
    s_actionList->PushBack(new GizmoTranslate(transforms));
    UpdateActions();
#ifdef _DEBUG
    editor::DisplayWarning("Translation Action");
#endif
    break;
  //Scale
  case gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE::EVT_SCL:
    s_actionList->PushBack(new GizmoScale(transforms));
    UpdateActions();
#ifdef _DEBUG
    editor::DisplayWarning("Scale Action");
#endif
    break;
  //Rotation
  case gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE::EVT_ROT:
    s_actionList->PushBack(new GizmoRotate(transforms));
    UpdateActions();
#ifdef _DEBUG
    editor::DisplayWarning("Rotation Action");
#endif
    break;
  default:
    editor::DisplayError("Manipulation type can not be handled by action manager");
  }
}

//Appends to the occuring manipulation action
void ActionManager::e_manipulating(dataStructures::Array<hndl> &transforms,
  gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE type)
{
  //Translation
  if (type == gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE::EVT_TRANS)
  {
    const GizmoTranslate *lastTrans = reinterpret_cast<const GizmoTranslate*>(s_actionList->m_tail->m_action);
    lastTrans->Append(transforms);
  }
  //Scale
  else if (type == gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE::EVT_SCL)
  {
    const GizmoScale *lastScale = reinterpret_cast<const GizmoScale*>(s_actionList->m_tail->m_action);
    lastScale->Append(transforms);
  }
  //Rotation
  else if (type == gizmoSpace::GizmoManager::GizmoEvent::EVENT_TYPE::EVT_ROT)
  {
    const GizmoRotate *lastRot = reinterpret_cast<const GizmoRotate*>(s_actionList->m_tail->m_action);
    lastRot->Append(transforms);
  }
  else
    editor::DisplayError("Manipulation type can not be handled by action manager");
}

//Focuses an action in the GameWidget
void ActionManager::SetFocus(void) const
{
  ActionList::ActionNode *current = s_actionList->m_current;
  if (current != nullptr)
  {
    if (current->m_action != nullptr)
    {
      current->m_action->Focus();
#ifdef _DEBUG
      editor::DisplayWarning("Focus " + current->m_action->GetLabel());
#endif
    }
  }
}

//Removes children from an object
void ActionManager::RemoveChildren(const dataStructures::Array<hndl> &objHndls,
  std::list<hndl> &objToRemove) const
{
  int size = objHndls.GetSize();
  for (int i = 0; i < size; ++i)
    objToRemove.push_back(objHndls[i]);
  
  for (int i = 0; i < size; ++i)
  {
    auto &obj = GET_OBJECT_MANAGER->GetObject(objHndls[i]);
    for (auto &it = objToRemove.begin(); it != objToRemove.end();)
    {
      //Remove all children
      auto &child = GET_OBJECT_MANAGER->GetObject(*it);
      if (obj.m_index != child.m_index)
      {
        if (obj.HasDescendant(*it))
        {
          it = objToRemove.erase(it);
          continue;
        }
      }

      ++it;
    }
  }
}

//Create dircetoies for action manager
void ActionManager::CreateDirectories(void)
{
  //Remove directory if exists
  char buffer[255];
  struct stat status;
  stat(s_tempFilesFolder.c_str(), &status);
  GetCurrentDirectory(255, buffer);
  if (status.st_mode & S_IFDIR)
    DeleteDirectory(std::string(buffer) + "\\" + s_tempFilesFolder, true);

  std::string dir = std::string(buffer) + "\\TempEditorFiles";
  CreateDirectory(dir.c_str(), NULL);
  CreateDirectory((dir + "\\" + s_tempActions).c_str(), NULL);

  dir += "\\" + s_tempActions + "\\";
  for (int i = 0; i < c_numOfActions; ++i)
  {
    std::string dirName = "Action" + std::string(_itoa(i, buffer, 10));
    m_actionDirPool[dirName] = true;
    std::string dirToCreate = dir + dirName;
    CreateDirectory(dirToCreate.c_str(), NULL);
  }
}

int ActionManager::DeleteDirectory(const std::string filePath, bool deleteSubDirs) const
{
  bool            bSubdirectory = false;       // Flag, indicating whether
                                               // subdirectories have been found
  HANDLE          hFile;                       // Handle to directory
  std::string     strFilePath;                 // Filepath
  std::string     strPattern;                  // Pattern
  WIN32_FIND_DATA FileInformation;             // File information


  strPattern = filePath + "\\*.*";
  hFile = ::FindFirstFile(strPattern.c_str(), &FileInformation);
  if (hFile != INVALID_HANDLE_VALUE)
  {
    do
    {
      if (FileInformation.cFileName[0] != '.')
      {
        strFilePath.erase();
        strFilePath = filePath + "\\" + FileInformation.cFileName;

        if (FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
          if (deleteSubDirs)
          {
            // Delete subdirectory
            int iRC = DeleteDirectory(strFilePath, deleteSubDirs);
            if (iRC)
              return iRC;
          }
          else
            bSubdirectory = true;
        }
        else
        {
          // Set file attributes
          if (::SetFileAttributes(strFilePath.c_str(),
            FILE_ATTRIBUTE_NORMAL) == FALSE)
            return GetLastError();

          // Delete file
          if (::DeleteFile(strFilePath.c_str()) == FALSE)
            return ::GetLastError();
        }
      }
    } while (::FindNextFile(hFile, &FileInformation) == TRUE);

    // Close handle
    ::FindClose(hFile);

    DWORD dwError = ::GetLastError();
    if (dwError != ERROR_NO_MORE_FILES)
      return dwError;
    else
    {
      if (!bSubdirectory)
      {
        // Set directory attributes
        if (::SetFileAttributes(filePath.c_str(),
          FILE_ATTRIBUTE_NORMAL) == FALSE)
          return ::GetLastError();

        // Delete directory
        if (::RemoveDirectory(filePath.c_str()) == FALSE)
          return ::GetLastError();
      }
    }
  }

  return 0;
}

//Gets directory t host action management
const std::string& ActionManager::GetAvailableSaveDirectory(void) const
{
  for (auto &it = m_actionDirPool.cbegin(); it != m_actionDirPool.cend(); ++it)
  {
    if (it->second == true)
      return it->first;
  }

  editor::DisplayError("Error occured: No directories to save action left!");
  return c_invalidDir;
}

//Free directory for usage
void ActionManager::SetActionDirAvailable(const std::string &dir)
{
  if (m_actionDirPool.find(dir) != m_actionDirPool.end())
    m_actionDirPool[dir] = true;
  else
    editor::DisplayError("Error occured trying to release action save directory!");
}

//Update toolbar
void ActionManager::UpdateActions(void)
{
  if (s_actionList->IsUndoAvailable())
    editor::g_actionManager->m_undoButton->EnableButton();
  else
    editor::g_actionManager->m_undoButton->DisableButton();

  if (s_actionList->IsRedoAvailable())
    editor::g_actionManager->m_redoButton->EnableButton();
  else
    editor::g_actionManager->m_redoButton->DisableButton();
}
