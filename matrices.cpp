#include<iostream>
#include<cmath>
#include<iomanip>

//Functions
void Ref(float **matrix, int order);
int FindNonZeroElem(float **matrix, int column, int order);
void ReduceEntriesBelow(float **matrix, int row, int column, int order);
void SwapRows(float **matrix, int row1, int row2, int order);
int FindPivot(float **matrix, int row, int order);
void Rref(float **matrix, int order);
void ReduceUnderPivot(float **matrix, int row, int column);
float Det(float **matrix, int order);
float Cofactor(float **matrix, int row, int column, int order);
void Adjoint(float **adjoint, float **matrix, int order);
void PrintMatrix(float **matrix, int order);
bool Inverse(float **inverse, float **adjoint, float det, int order);
float *SolveCramer(float **matrix, float *b, float det, int order);
void CopyMatrix(float **copy, float **source, int order);

int main(void)
{ 
  int order;
  float det;
  float *b;

  std::cout << "Enter size of squared matrix: ";
  std::cin >> order;
  
  //Allocate memory for general matrix
  float **matrix = new float*[order];
  for (int i = 0; i < order; ++i)
    matrix[i] = new float[order];

  //Allocate memory to hold inverse
  float **inverse = new float*[order];
  for (int i = 0; i < order; ++i)
    inverse[i] = new float[order];

  //Allocate memory fo adjoint
  float **adjoint = new float*[order];
  for (int i = 0; i < order; ++i)
    adjoint[i] = new float[order];

  //Input
  for (int i = 0; i < order; ++i)
  {
    std::cout << "Enter " << i + 1 << "row entries separated by SPACEs: ";
    for (int j = 0; j < order; ++j)
      std::cin >> matrix[i][j];
  }
  //Print matrix we entered
  std::cout << std::endl;
  std::cout << "You entered matrix:\n";
  PrintMatrix(matrix, order);

  det = Det(matrix, order);
  std::cout << "Determinant is " << det << std::endl;

  if (det != 0)
  {
    std::cout << "Adjoint matrix:\n";
    Adjoint(adjoint, matrix, order);
    PrintMatrix(adjoint, order);
    Inverse(inverse, adjoint, det, order);
    std::cout << "Inverse:\n";
    PrintMatrix(inverse, order);
  }
  else
    std::cout << "Matrix doesn't have an inverse\n";
  
  if (det != 0)
  {
    std::cout << "Enter entries of vector b separated by SPACEs: ";
    b = new float[order];
    for (int i = 0; i < order; ++i)
      std::cin >> b[i];

    float *result = SolveCramer(matrix, b, det, order);
    std::cout << "Solution: " << std::endl;

    for (int i = 0; i < order; ++i)
    {
      std::cout.precision(2);
      std::cout.setf(std::ios::fixed, std::ios::floatfield);
      std::cout.width(4);
      std::cout << result[i] << " ";
    }
    std::cout << std::endl;

    delete[] result;
    delete[] b;
  }
  //Ref
  //Ref(matrix, order);

  //std::cout << "REF matrix:\n";
  //PrintMatrix(matrix, order);
  ////RREF
  //Rref(matrix, order);

  //std::cout << "RREF matrix:\n";
  //PrintMatrix(matrix, order);

  for (int i = 0; i < order; ++i)
  {
    delete[] matrix[i];
    delete[] inverse[i];
    delete[] adjoint[i];
  }
  delete[] matrix;
  delete[] inverse;
  delete[] adjoint;

  system("pause");
  return 0;
}

void Ref(float **matrix, int order)
{
  int row;

  for (int i = 0, j = 0; i < order && j < order; ++i, ++j)                         
  {
    row = FindNonZeroElem(matrix, j, order);
    //If column has only zero entries go to next one
    if (row == -1)
      continue;
    ReduceEntriesBelow(matrix, row , j, order);
    //Swap rows if previous one had zero entry
      if (row != i)
        SwapRows(matrix, row, row - 1, order);
  }

}

int FindNonZeroElem(float **matrix, int column, int order)
{
  for (int i = 0; i < order; ++i)
    if (matrix[column][i] != 0)
      return i;

  return -1;
}

void ReduceEntriesBelow(float **matrix, int row, int column, int order)
{
  float factor = matrix[row][column];

  //Get a pivot
  for (int i = 0; i < order; ++i)
    matrix[row][i] /= factor;
  //If this row is the last retrun
  if (row == order - 1)
    return;

  //Reduction
  for (int i = row + 1; i < order; ++i)
  {
    factor = matrix[i][row];
    for (int j = 0; j < order; ++j)
      matrix[i][j] -= matrix[row][j] * factor;
  }
}

void SwapRows(float **matrix, int row1, int row2, int order)
{
  float temp;

  for (int i = 0; i < order; ++i)
  {
    temp = matrix[row1][i];
    matrix[row1][i] = matrix[row2][i];
    matrix[row2][i] = temp;
  }
}

void Rref(float **matrix, int order)
{
  for (int i = order - 1; i >= 0; i--)
  {
    int column = FindPivot(matrix, i, order);
    if (column == -1)
      continue;
    ReduceUnderPivot(matrix, i, column);
  }
}

int FindPivot(float **matrix, int row, int order)
{
  for (int i = order - 1; i >= 0; --i)
  {
    if (matrix[row][i] == 1)
      return i;
  }

  return -1;
}

void ReduceUnderPivot(float **matrix, int row, int column)
{
  for (int i = row - 1; i >= 0; --i)
  {
    float factor = matrix[i][column];
    matrix[i][column] -= factor * matrix[row][column];
  }
}

float Det(float **matrix,int order)
{
  float sum = 0;
  //First order case
  if (order == 1)
    return matrix[0][0];
  //Second order case
  if (order == 2)
    return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
  else
  {
    //expanding row, thus looping through columns
    for (int expColumn = 0; expColumn < order; ++expColumn)
    {
      int tempRow = 0, tempColumn = 0;
      //creating minor
      float **minor = new float*[order];
      for (int i = 0; i < order; ++i)
        minor[i] = new float[order];
      //minor row
      for (int minRow = 1; minRow < order; ++minRow)
      {
        //minor column
        for (int minColumn = 0; minColumn < order; ++minColumn)
        {
          //skip if it is a column we are expanding
          if (expColumn == minColumn)
            continue;
          //saving minor
          minor[tempRow][tempColumn++] = matrix[minRow][minColumn];
        }

        ++tempRow;
        tempColumn = 0;
      }
      //Recursively call det() of lower order matrix
      sum += matrix[0][expColumn] * (float)pow(-1, expColumn)*Det(minor, order - 1);
      delete[] minor;
    }
    return sum;
  }
}

float Cofactor(float **matrix, int row, int column, int order)
{
  int minRow = 0, minColumn = 0;
  float cofactor;

  float **minor = new float*[order - 1];
  for (int i = 0; i < order - 1; ++i)
    minor[i] = new float[order - 1];

  for (int i = 0; i < order; ++i)
  {
    //Skip expanding row
    if (i == row)
      continue;

    for (int j = 0; j < order; ++j)
    {
      //Sjip expanding column
      if (j != column)
        minor[minRow][minColumn++] = matrix[i][j];
    }
    ++minRow;
    minColumn = 0;
  }

  cofactor = (float)pow(-1, (row+1) + (column+1)) * Det(minor, order - 1);

  for (int i = 0; i < order - 1; ++i)
    delete[] minor[i];
  delete[] minor;

  return cofactor;
}

void Adjoint(float **adjoint, float **matrix, int order)
{
  for (int i = 0; i < order; ++i)
    for (int j = 0; j < order; ++j)
      adjoint[j][i] = Cofactor(matrix, i, j, order);
}

void PrintMatrix(float **matrix, int order)
{
  for (int i = 0; i < order; ++i)
  {
    for (int j = 0; j < order; ++j)
    {
      std::cout.precision(2);
      std::cout.setf(std::ios::fixed, std::ios::floatfield);
      std::cout.width(4);
      std::cout <<std::left << (matrix[i][j] ? matrix[i][j] : fabs(matrix[i][j])) << " ";
    }
    std::cout << "\n\n";
  }
}

bool Inverse(float **inverse, float **adjoint, float det, int order)
{

  for (int i = 0; i < order; ++i)
    for (int j = 0; j < order; ++j)
      inverse[i][j] = adjoint[i][j] / det;

  return true;
}

float *SolveCramer(float **matrix, float *b, float det, int order)
{
  float *solution = new float[order];
  float **tempMatrix;

  tempMatrix = new float*[order];
  for (int i = 0; i < order; ++i)
    tempMatrix[i] = new float[order];

  for (int k = 0; k < order; ++k)
  {
    CopyMatrix(tempMatrix, matrix, order);
    for (int i = 0; i < order; ++i)
    {
      tempMatrix[i][k] = b[i];
    }
    float crDet = Det(tempMatrix, order);
    solution[k] = crDet / det;
  }

  for (int i = 0; i < order; ++i)
    delete[] tempMatrix[i];
  delete[] tempMatrix;

  return solution;
}

void CopyMatrix(float **copy, float **source, int order)
{
  for (int i = 0; i < order; ++i)
    for (int j = 0; j < order; ++j)
      copy[i][j] = source[i][j];
}
